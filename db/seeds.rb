# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Person.create(first_name: 'Troy', last_name: 'Murray')
Person.create(first_name: 'Andrea', last_name: 'Murray')
Person.create(first_name: 'Miles', last_name: 'Murray')
Person.create(first_name: 'Homer', last_name: 'Simpson')
Person.create(first_name: 'Luke', last_name: 'Skywalker')
Person.create(first_name: 'Bart', last_name: 'Simpson')
Person.create(first_name: 'Bruce', last_name: 'Wayne')
