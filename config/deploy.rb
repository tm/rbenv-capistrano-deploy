require "bundler/capistrano"

load "config/recipes/base"
load "config/recipes/rbenv"
load "config/recipes/nodejs"
load "config/recipes/nginx"
load "config/recipes/unicorn"
load "config/recipes/check"

server "1.1.1.1", :web, :app, :db, primary: true
set :server_name, "localhost"
set :port, 22
set :user, "deployer"
set :application, "rbenv-capistrano-deploy"
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, "git"
set :repository, "git@gitlab.msu.edu:user/#{application}.git"
set :branch, "master"  # master

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

after "deploy", "deploy:cleanup" # keep only the last 5 releases

set :default_environment, {
  'PATH' => "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
}