namespace :rbenv do
  desc "Install rbenv"
  task :install do
    run "git clone git://github.com/sstephenson/rbenv.git ~/.rbenv"
    run "echo 'export PATH=\"$HOME/.rbenv/bin:$PATH\"' >> ~/.profile"
    run "echo 'eval \"$(rbenv init -)\"' >> ~/.profile"
    run "git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build"
  end
  after "deploy:install", "rbenv:install"

  desc "Install ruby"
  task :install_ruby do
    run "rbenv install 1.9.3-p374"
    run "rbenv rehash"
    run "rbenv global 1.9.3-p374"
    run "gem install bundler --no-rdoc --no-ri"
    run "rbenv rehash"
  end
  after "rbenv:install", "rbenv:install_ruby"

end