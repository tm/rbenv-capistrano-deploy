namespace :logrotate do
  desc "Setup logrotate task for application log files"
  task :setup_app_logs, roles: :app do
    template "logrotate.erb", "tmp/#{application}"
    run "#{sudo} mv -f tmp/#{application} /etc/logrotate.d"
  end
  after "deploy:cold", "logrotate:setup_app_logs"

  desc "Setup logrotate task for web server log files"
  task :setup_web_logs, roles: :web do
    template "logrotate.erb", "tmp/#{application}"
    run "#{sudo} mv -f tmp/#{application} /etc/logrotate.d"
  end
  after "deploy:cold", "logrotate:setup_web_logs"
end