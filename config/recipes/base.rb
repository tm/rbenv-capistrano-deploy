def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end

namespace :deploy do
  desc "Install everything onto the server"
  task :install do
    run "#{sudo} apt-get -y update"
    run "#{sudo} apt-get -y install python-software-properties git freetds-bin freetds-common freetds-dev libct4 libsybdb5 tdsodbc curl build-essential gcc libfontconfig bash patch bzip2 openssl libreadline6 libreadline6-dev zlib1g zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev autoconf libc6-dev libgdbm-dev libncurses5-dev automake libtool bison subversion pkg-config libffi-dev dkms"
    run "#{sudo} apt-get -y upgrade"
  end

  desc "Move database.example.yml file to database.yml in shared location"
  task :setup_database_config do
    run "mkdir -p #{shared_path}/config/"
    put File.read("config/database.yml"), "#{shared_path}/config/database.yml"
    puts ""
    puts "------------------------------------------------------------------------"
    puts "------------------------------------------------------------------------"
    puts "-----------------------------           --------------------------------"
    puts "----------------------------- IMPORTANT --------------------------------"
    puts "-----------------------------           --------------------------------"
    puts "------------------------------------------------------------------------"
    puts "------------------------------------------------------------------------"
    puts "Now edit the database config file in #{shared_path}/config/database.yml."
    puts ""
    puts ""
    puts ""
    puts ""
    puts ""
  end
  after "deploy:setup", "deploy:setup_database_config"

  task :symlink_config, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
  after "deploy:finalize_update", "deploy:symlink_config"

end