namespace :nodejs do
  desc "Install the latest relase of Node.js"
  task :install, roles: :app do
    run "#{sudo} add-apt-repository ppa:chris-lea/node.js --yes", :shell => 'bash'
    run "#{sudo} apt-get -y update", :shell => 'bash'
    run "#{sudo} apt-get -y install nodejs", :shell => 'bash'
  end
  after "deploy:install", "nodejs:install"
end