# Rails App Deployment Example

This is an example application to deploy a Ruby on Rails application to a remote server.  This includes the following tasks:

1. Update the server to the latest package releases
2. Install additional packages needed for deployment
3. Configure the server local firewall
4. Install the nginx web server
5. Install the node.js Javascript runtime library
6. Configure the nginx web server for the application
7. Configure the unicorn application server for the application


## Requirements

1. Ruby 1.9.3 or newer
2. A server to deploy to running Ubuntu Server 12.04 with OpenSSH installed
3. A local user account on the server in the admin group


## Getting Started

1. Open the config/deploy.rb file
2. Update the IP address to reflect the server you'll be deploying to
3. Update the server name with a fully qualifed domain name
4. Update the port number if you use a different SSH port
5. Set the application name
6. Set the repository address and branch to deploy from.
7. Change to the local directory where this application is
8. Run ``bundle update``
9. Run ``cap deploy:install`` to install needed packages
10. Run ``cap deploy:setup`` to setup the server
11. Run ``cap deploy:cold`` for the first time you deploy the application

Note that all subsequent deployments should be run using ``cap deploy`` only.


## Caveat

Because this is an example application it uses sqlite as the production database on the server.  Because of this it's overwritten with each deployment.  You'll also need to trigger the migrations to create it using ``cap deploy:migrations``

