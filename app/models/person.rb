class Person < ActiveRecord::Base
  attr_accessible :date_of_birth, :first_name, :job_title, :last_name
end
